
/**
* 	This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU Lesser General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU Lesser General Public License for more details.
*
*   You should have received a copy of the GNU Lesser General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/
*   
*   Copyright 2013 Mathieu DUPRIEZ <contact@mdudev.com>
*/

package com.mdudev.utils.properties;

import java.util.Enumeration;
import java.util.Properties;

public class PropertiesHelper {
	
	private PropertiesHelper(){}
	
	  /**
	   * Merge two sets of properties
	   * 
	   * @param First set of properties to merge
	   * @param Second set of properties to merge
	   * @return Merged set of properties A and B
	   */
	public static Properties merge(Properties setA, Properties setB) {
		
		// Initialize the merged set
		Properties mergedSet = new Properties();
		
		// Iterate through setA keys
		Enumeration<?> e = setA.propertyNames();
		while(e.hasMoreElements())
		{
			// Add each A key/value to the merged set
			String lKey = (String)e.nextElement();
			mergedSet.setProperty(lKey, setA.getProperty(lKey));
		}
		
		// Iterate through setB keys
		e = setB.propertyNames();
		while(e.hasMoreElements())
		{
			// Add each B key/value to the merged set
			String lKey = (String)e.nextElement();
			mergedSet.setProperty(lKey, setB.getProperty(lKey));
		}
		
		return mergedSet;
	}
	
	  /**
	   * Extract a set of properties with the specified key prefix
	   * 
	   * @param First set of properties to merge
	   * @param Second set of properties to merge
	   * @return Merged set of properties A and B
	   */
	public static Properties extract(Properties setA, String keyPrefix, boolean keepPrefix) {
		
		// Initialize the merged set
		Properties extractedSet = new Properties();
		
		// Iterate through setA keys
		Enumeration<?> e =  setA.propertyNames();
		while(e.hasMoreElements())
		{
			// Read current key
			String lKey = (String)e.nextElement();
			
			// Check if key match with prefix
			if(lKey.startsWith(keyPrefix))
			{
				if(keepPrefix)
				{
					// Appends new entry with prefix
					extractedSet.setProperty(lKey, setA.getProperty(lKey));
				}
				else
				{
					// Appends new entry without prefix
					extractedSet.setProperty(lKey.substring(keyPrefix.length() + 1), setA.getProperty(lKey));
				}
				
				
			}
			else
			{
				// Nothing to do ...
			}
		}
		
		return extractedSet;
	}
}
