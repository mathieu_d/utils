
/**
* 	This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU Lesser General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU Lesser General Public License for more details.
*
*   You should have received a copy of the GNU Lesser General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/
*   
*   Copyright 2013 Mathieu DUPRIEZ <contact@mdudev.com>
*/

package com.mdudev.utils.jar;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

public class JarHelper {
	
	private JarHelper(){}
	
	  /**
	   * Retrieves the path of the jar containing the input class
	   * 
	   * @param Class inside the jar
	   * @return File pointing to the directory where is located the jar
	   */
	public static File getJarDir(Class<?> clazz) {
		
		// Get the URL of clazz
		URL url = clazz.getResource(clazz.getSimpleName() + ".class");

		// Convert URL to external form
	    String extURL = url.toExternalForm();
	    
	    // At this point extURL looks like
	    // file:/Path/Of/The/Jar/clazz.class or
	    // jar:file:/Path/Of/The/Jar/jar.jar!/package/of/the/class/clazz.class
	    
	    // Retrieves the suffix that contain the package of the class
        String suffix = "/"+(clazz.getName()).replace(".", "/")+".class";
        // Remove the suffix
        extURL = extURL.replace(suffix, "");

	    // At this point extURL looks like
	    // file:/Path/Of/The/Jar or
	    // jar:file:/Path/Of/The/Jar/jar.jar!
        
        // Looks if the URL contains jar! and remove everything before the last /
        if (extURL.startsWith("jar:") && extURL.endsWith(".jar!"))
            extURL = extURL.substring(4, extURL.lastIndexOf("/"));
        

        // Convert external form to URL
	    try {
	        url = new URL(extURL);
	    } catch (MalformedURLException mux) {
	    	// Nothing to do ...
	    }

	    // Convert URL to File and return File
	    try {
	        return new File(url.toURI());
	    } catch(URISyntaxException ex) {
	        return new File(url.getPath());
	    }
	}
}
