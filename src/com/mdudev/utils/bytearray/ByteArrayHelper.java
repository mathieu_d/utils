
/**
* 	This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU Lesser General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU Lesser General Public License for more details.
*
*   You should have received a copy of the GNU Lesser General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/
*   
*   Copyright 2013 Mathieu DUPRIEZ <contact@mdudev.com>
*/

package com.mdudev.utils.bytearray;

public class ByteArrayHelper {
	
	private ByteArrayHelper(){}
	
	/**
	 * Retrieves a bit inside byte array
	 * 
	 * @param Byte array
	 * @param Bit position inside the byte array
	 * @return Bit value
	 */
	public static byte getBit(byte[] data, int pos)
	{
		int bytePos = pos / 8;
		
		if(bytePos < data.length)
		{
			if((data[bytePos] & (1 << pos-(bytePos * 8))) == 0)
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}
			
		return 0;
	}
	
	/**
	 * Set a bit inside byte array
	 * 
	 * @param Byte array
	 * @param Bit position inside the byte array
	 */
	public static void setBit(byte[] data, int pos)
	{
		int bytePos = pos / 8;
		
		if(bytePos < data.length)
		{
			data[bytePos] = (byte) (data[bytePos]  | (1 << pos-(bytePos * 8)));
		}
		else
		{
			// Nothing to do 
		}
	}
	
	/**
	 * Clear a bit inside byte array
	 * 
	 * @param Byte array
	 * @param Bit position inside the byte array
	 */
	public static void clearBit(byte[] data, int pos)
	{
		int bytePos = pos / 8;
		
		if(bytePos < data.length)
		{
			data[bytePos] = (byte) (data[bytePos]  & ~(1 << pos-(bytePos * 8)));
		}
		else
		{
			// Nothing to do 
		}
	}
	
	/**
	 * Extract a bit field inside byte array
	 * 
	 * @param Byte array
	 * @param Start position of the bit field in bits 
	 * @param Length of the bit field
	 * @return Byte array containing the bit field
	 */
	public static byte[] extract(byte[] data,int start ,int len)
	{
		if(start + len - 1  < data.length*8)
		{
			byte[] b = new byte[((len-1)/8)+1];
			for(int i = 0 ; i < len ; i++)
			{
				if(getBit(data,i + start) == 0)
				{
					clearBit(b,i);
				}
				else
				{
					setBit(b,i);
				}
			}
			
			return b;
		}
		else
		{
			// Nothing to do ...
		}
		
		return new byte[0];
	}
	
	/**
	 * Copy a bit field inside byte array
	 * 
	 * @param Byte array destination
	 * @param Bit field source as byte array
	 * @param Start position of the bit field in bits 
	 * @param Length of the bit field
	 */
	public static void copy(byte[] src,byte[] dest,int start ,int len)
	{
		if((start + len - 1  < dest.length*8) && (len-1 < src.length*8))
		{
			for(int i = 0 ; i < len ; i++)
			{
				if(getBit(src,i) == 0)
				{
					clearBit(dest,i + start);
				}
				else
				{
					setBit(dest,i + start);
				}
			}

		}
		else
		{
			// Nothing to do ...
		}
	}
}
